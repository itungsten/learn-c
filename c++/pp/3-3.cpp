#include <iostream>
using namespace std;
int main(int argc, char const *argv[])
{
	const  float change = 60;
	cout << "Enter a latitude in degrees,minutes, and seconds\n"
		<< "First,enter the degrees : ";
	float degrees;
	cin >> degrees;
	cout << "Next , enter the minutes of arc : ";
	float minutes;
	cin >> minutes;
	cout << "Finally, enter the seconds of arc : ";
	float seconds;
	cin >> seconds;
	cout << degrees << " degrees, " << minutes << " minutes, " << seconds << " seconds = " << degrees+minutes/change+seconds/change/change << " degrees\n";
	return 0;
}